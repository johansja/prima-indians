defmodule PimaIndians do
  use Application
  require Logger

  def start(_type, _args) do
    filename = "priv/pima-indians-diabetes.data.csv"
    split_ratio = 0.67
    dataset = load_csv(filename)
    {trainingset, testset} = split_dataset(dataset, split_ratio)

    Logger.info(
      "Split #{length(dataset)} into train=#{length(trainingset)} and test=#{length(testset)} rows"
    )

    summaries = summarize_by_class(trainingset)
    predictions = get_predictions(summaries, testset)
    accuracy = get_accuracy(testset, predictions)
    Logger.info("Accurancy: #{accuracy}")

    {:error, "finished"}
  end

  def load_csv(filename) do
    filename
    |> File.stream!()
    |> CSV.decode!()
    |> Stream.map(fn row ->
      Enum.map(row, fn value ->
        {f, _binary} = Float.parse(value)
        f
      end)
    end)
    |> Enum.into([])
  end

  def split_dataset(dataset, split_ratio) do
    train_size = trunc(length(dataset) * split_ratio)

    dataset
    |> Enum.shuffle()
    |> Enum.split(train_size)
  end

  def separate_by_class(dataset) do
    Enum.group_by(dataset, fn x -> Enum.at(x, -1) end)
  end

  def summarize(dataset) do
    dataset
    |> Enum.zip()
    |> Enum.drop(-1)
    |> Enum.map(fn values ->
      values = Tuple.to_list(values)
      {Numerix.Statistics.mean(values), Numerix.Statistics.std_dev(values)}
    end)
  end

  def summarize_by_class(dataset) do
    dataset
    |> separate_by_class()
    |> Enum.map(fn {class, instances} -> {class, summarize(instances)} end)
    |> Enum.into(%{})
  end

  def calculate_probability(x, mean, std_dev) do
    exponent = :math.exp(-(:math.pow(x - mean, 2) / (2 * :math.pow(std_dev, 2))))
    1 / (:math.sqrt(2 * :math.pi()) * std_dev) * exponent
  end

  def calculate_class_probabilities(summaries, input_vector) do
    summaries
    |> Enum.map(fn {class, class_summaries} ->
      probability =
        class_summaries
        |> Enum.zip(input_vector)
        |> Enum.reduce(1, fn {{mean, std_dev}, x}, acc ->
          acc * calculate_probability(x, mean, std_dev)
        end)

      {class, probability}
    end)
    |> Enum.into(%{})
  end

  def predict(summaries, input_vector) do
    {class, _} =
      summaries
      |> calculate_class_probabilities(input_vector)
      |> Enum.max_by(fn {_, probability} -> probability end)

    class
  end

  def get_predictions(summaries, testset) do
    Enum.map(testset, fn x -> predict(summaries, x) end)
  end

  def get_accuracy(testset, predictions) do
    correct =
      testset
      |> Enum.zip(predictions)
      |> Enum.count(fn {test, predict} ->
        Enum.at(test, -1) == predict
      end)

    correct / length(testset) * 100
  end
end
